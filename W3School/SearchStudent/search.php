<!DOCTYPE html>
<html>
<body onload="loadDoc('','demo')">

<h1>Input</h1>
<input type="text" id="givenInput" onkeyup="loadDoc(this.value,'demo')">
<br><br>
<h1>Output</h1>
<p id="demo"></p>

<script>
    var xhttp;
    if(window.XMLHttpRequest){
        //for new version of browser
        xhttp=new XMLHttpRequest();
    }else{
        //for old version of IE browser(IE 5 and IE 6)
        xhttp=new ActiveXObject('Microsoft.XMLHTTP')
    }
    function loadDoc(givenInput, outputId) {
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText;
            }
        };
        searchPage = 'Student.php?text=' + givenInput;
        xhttp.open('GET', searchPage);
        xhttp.send();
    }
    function update(id) {
        var name=document.getElementById('name'+id).innerHTML;
        var email=document.getElementById('email'+id).innerHTML;
        var mobile=document.getElementById('mobile'+id).innerHTML;

        searchPage = 'Student.php?id=' + id+'&name='+name+'&email='+email+'&mobile='+mobile;
        xhttp.open('GET', searchPage);
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("name").innerHTML = this.responseText;
                document.getElementById("email").innerHTML = this.responseText;
                document.getElementById("mobile").innerHTML = this.responseText;
            }
        };
        xhttp.send();
    }
    function countLike(id,demo) {
        searchPage = 'Student.php?id=' + id;
        xhttp.open('GET', searchPage);
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText;
            }
        };
        xhttp.send();
    }
</script>

</body>
</html>
<?php
namespace App\W3School\SearchStudent;

use PDO;

class Student
{
    public $host = 'localhost';
    public $user = 'root';
    public $pass = '';
    public $db = 'student';
    public $conn = '';
    public $id = '';
    public $name = '';
    public $mobile = '';
    public $email = '';
    public $text = '';
    public $like = '';

    public function __construct()
    {

        try {
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = '')
    {
        if (array_key_exists('text', $data) and !empty($data)) {
            $this->text = $data['text'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('email', $data) and !empty($data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('mobile', $data) and !empty($data)) {
            $this->mobile = $data['mobile'];
        }
        if (array_key_exists('like', $data) and !empty($data)) {
            $this->like = $data['like'];
        }
    }
    public function index()
    {
        $sql = "SELECT * FROM `student`";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function search($searchStd)
    {
        $sqlquery = "SELECT * FROM `student` WHERE `name` LIKE '%" . $searchStd . "%' OR `mobile` LIKE '%" . $searchStd . "%' OR `email` LIKE '%" . $searchStd . "%'";
        $stmt = $this->conn->prepare($sqlquery);
        $stmt->execute();
        $singledata = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $singledata;
    }

    public function update()
    {
        $sql = "UPDATE `student` SET `name`=:name, `email`=:email, `mobile`=:mobile WHERE `id` = :id;";
        $getSql = $this->conn->prepare($sql);
        $result=$getSql->execute(array(
            ":name"=>$this->name,
            ":email"=>$this->email,
            ":mobile"=>$this->mobile,
            ":id"=>$this->id
        ));
        return $result;
    }
    public function view()
    {
        $sql = "SELECT * FROM `student` WHERE `id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        $singleResult = $getSql->fetch(PDO::FETCH_ASSOC);
        return $singleResult;
    }
    public function countLike()
    {
        $sql = "UPDATE `student` SET `like`=:like WHERE `id` = :id;";
        $getSql = $this->conn->prepare($sql);
        $result=$getSql->execute(array(
            ":like"=>$this->like,
            ":id"=>$this->id
        ));
        return $result;
    }

}
$obj = new Student();
if (isset($_GET['text'])) {
    $obj->setData($_GET);
    $res = $obj->search($_GET['text']);
} else {
    $res = $obj->index();
}
if (!isset($_GET['text'])){
    $obj->setData($_GET);
    $obj->update();
}
if (!isset($_GET['text'])){
    $singleStd=$obj->view();
    $_GET['like']=$singleStd['like']+1;
    $obj->setData($_GET);
    $obj->countLike();
}
?>
<table border="1" id="tableHolder">
    <tr align="center">
        <td>ID</td>
        <td>Name</td>
        <td>Email</td>
        <td>Mobile</td>
        <td>Action</td>
    </tr>
    <?php
    foreach ($res as $value) {
        ?>
        <tr align="center">
            <td><?php echo $value['id']; ?></td>
            <td contenteditable="true" id="name<?php echo $value['id']; ?>"
                onblur="update(<?php echo $value['id']; ?>)"><?php echo $value['name']; ?></td>
            <td contenteditable="true" id="email<?php echo $value['id']; ?>"
                onblur="update(<?php echo $value['id']; ?>)"><?php echo $value['email']; ?></td>
            <td contenteditable="true" id="mobile<?php echo $value['id']; ?>"
                onblur="update(<?php echo $value['id']; ?>)"><?php echo $value['mobile']; ?></td>
            <td>(<?php echo $value['like']; ?>)<button onclick="countLike(<?php echo $value['id']; ?>,'demo')">Like</button></td>
        </tr>
    <?php } ?>
</table>




